### What is this repository for? ###

* Tested tactics against 3.68 release AI tactics for 100 season with same team tapani created for greek league.

* Each folder should contain .tct file -  zip of .tct file - New Microsoft Excel Worksheet.xlsx - benchresult.txt

* New Microsoft Excel Worksheet.xlsx  Should contain the cleaned benchresult.txt.

* screenshots folder an image of each tactic as tacticname.png

~~~

* Clean the benchresult.txt use the following;

* Use np++  https://notepad-plus-plus.org/ for marking lines with PAS Gianni only. (ctrl +f )

* Use np++ Search / Bookmark / Remove unmarked lines (set alt + d).

* Use np++ Line operations / Remove duplicate lines (set alt + a)

* Use np++ Replace C and R with space.

* Paste results to New Microsoft Excel Worksheet which has all formulas, move the tactic folder to (tested tactics) folder.

* Refresh (CM Tactics - Source 1) from Query tab.

* Update CM Tactics - Source 2, 3 and 4

* CM Tactics Sheet2 statics needs updated but not all columns due to it takes very long for calculations! (I left only first and last line with calculation formulas)


~~~


* You may also use EditPad Lite https://www.editpadlite.com/ , line options on search panel for replace C and R with space then cut matches for PAS Giannina (alt +f).

* Remove all content, paste only PAS Giannina lines.

* Delete dublicate lines (set alt + d) and Trim Whitespace (set alt + s) 

*  For shortcut settigns use; Preferences, Keyboard, Extre menu.

 Paste results to New Microsoft Excel Worksheet.xlsx which has all formulas, move the tactic folder to (tested tactics) folder.

* Refresh (CM Tactics - Source 1) from Query tab.

* Update CM Tactics - Source 2, 3 and 4

* CM Tactics Sheet2 statics needs updated but not all columns due to it takes very long for calculations! (I left only first and last line with calculation formulas)

~~~

* You may also use the tool created by tafo instead of some of the cleaning text and creating excel files, still some of the steps needs manual work.

*  https://github.com/tafo/TBL/tree/master/Release